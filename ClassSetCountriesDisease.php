<?//namespace timetable;

/**
 * Class ClassSetCountriesDisease
 * Добавление свойств страны для болезней и болезней для страны в зависимости от актуальных данных аналитики
 */
class ClassSetCountriesDisease
{
    /**
     * У 11 инфоблока(Страны) очистка свойств болезней и риска если есть
     */
    static function ClearDiseaseWithCountries()
    {
        if (CModule::IncludeModule("iblock")) {
            $el = new CIBlockElement;
            $arSelect = Array("ID");
            $arFilter = Array("IBLOCK_ID" => 11, "ACTIVE" => "Y", "!PROPERTY_COUNTRY" => false);
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
            while ($ob = $res->GetNextElement()) {
                $arFields = $ob->GetFields();
                $ELEMENT_ID = $arFields['ID'];  // код элемента
                $IBLOCK_ID = $arFilter['IBLOCK_ID'];
                $PROPERTY_CODE = "COUNTRY";  // код свойства
                $PROPERTY_VALUE = "";// значение свойства
                CIBlockElement::SetPropertyValues($ELEMENT_ID, $IBLOCK_ID, $PROPERTY_VALUE, $PROPERTY_CODE);
                $PROPERTY_CODE = "COUNTRY_TRANSIT";  // код свойства
                CIBlockElement::SetPropertyValues($ELEMENT_ID, $IBLOCK_ID, $PROPERTY_VALUE, $PROPERTY_CODE);
            }
        }
    }


    /**
     * У 16 инфоблока(Инфекционные болезни) очистка свойств страны и транзитные страны если есть
     */
    static function ClearCountriesWithDisease()
    {
        if (CModule::IncludeModule("iblock")) {
            $el = new CIBlockElement;
            $arSelect = Array("ID");
            $arFilter = Array("IBLOCK_ID" => 16, "ACTIVE" => "Y", "!PROPERTY_DISEASE" => false);
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
            while ($ob = $res->GetNextElement()) {
                $arFields = $ob->GetFields();
                $ELEMENT_ID = $arFields['ID'];  // код элемента
                $IBLOCK_ID = $arFilter['IBLOCK_ID'];
                $PROPERTY_CODE = "DISEASE";  // код свойства
                $PROPERTY_VALUE = " ";// значение свойства
                CIBlockElement::SetPropertyValues($ELEMENT_ID, $IBLOCK_ID, $PROPERTY_VALUE, $PROPERTY_CODE);
                $PROPERTY_CODE = "RISK";  // код свойства
                CIBlockElement::SetPropertyValues($ELEMENT_ID, $IBLOCK_ID, $PROPERTY_VALUE, $PROPERTY_CODE);
            }
        }
    }

    static function ClearTimetableWithRisk()
    {
        if (CModule::IncludeModule("iblock")) {
            $el = new CIBlockElement;
            $arSelect = Array("ID");
            $arFilter = Array("IBLOCK_ID" => 5, "ACTIVE" => "Y", "!PROPERTY_RISK" => false);
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
            while ($ob = $res->GetNextElement()) {
                $arFields = $ob->GetFields();
                $ELEMENT_ID = $arFields['ID'];  // код элемента
                $IBLOCK_ID = $arFilter['IBLOCK_ID'];
                $PROPERTY_CODE = "RISK";  // код свойства
                $PROPERTY_VALUE = "";// значение свойства
                CIBlockElement::SetPropertyValues($ELEMENT_ID, $IBLOCK_ID, $PROPERTY_VALUE, $PROPERTY_CODE);
            }
        }
    }

    /**
     * @global $mess;
     * @global $DB;
     * Добавление свойств страны для болезней и болезней для страны в зависимости от актуальных данных аналитики
     */
    static function SetPropertyCountriesDisease()
    {
        global $mess;
        global $DB;
        if (CModule::IncludeModule("iblock")) {
            $el = new CIBlockElement;

            $results = $DB->Query("
SELECT idAnalytics, nameDiseaseRu, nameCountryRu, dateAddAnalytics, levelRisk, lifeСycle, countryId, ARCHIVE
FROM (SELECT idAnalytics, nameDiseaseRu, bx_CN.Name as nameCountryRu, idCountry 
as countryId, dateAddAnalytics, bxd_a.levelRisk, lifeСycle, DATE_ADD( dateAddAnalytics, INTERVAL lifeСycle DAY ), 
			CASE WHEN NOW() > DATE_ADD(dateAddAnalytics, INTERVAL lifeСycle DAY) THEN 1 ELSE 0 END AS ARCHIVE
			FROM  `bx_disease_analytics` bxd_a
			LEFT JOIN  `bx_disease_disease` bxd_d ON bxd_d.idDisease = bxd_a.idDisease
			LEFT JOIN  (select CountryId, Name  from `CountryNames` where NameLanguage = 1 
			GROUP BY CountryId) bx_CN ON bx_CN.CountryId = bxd_a.idCountry) bx_a
		WHERE ARCHIVE = 0;");

            $name_array = array();
            while ($row = $results->Fetch()) {
                array_push($name_array, $row);
            }


            foreach ($name_array as $item) {
                $arSelect = Array("ID");
                $arFilter = Array("IBLOCK_ID" => 11, "ACTIVE" => "Y", "NAME" => $item['nameDiseaseRu']);
                $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                while ($ob = $res->GetNextElement()) {
                    $arFields = $ob->GetFields();
                    $id_disease = $arFields["ID"];
                }
                $arSelect = Array("ID");
                $arFilter = Array("IBLOCK_ID" => 16, "ACTIVE" => "Y", "PROPERTY_COUNTRIE_ID" => $item['countryId']);
                $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                while ($ob = $res->GetNextElement()) {
                    $arFields = $ob->GetFields();
                    $id_country = $arFields["ID"];
                }
                $arr_countries[$id_country]['DISEASE'][] = $id_disease;
                $arr_countries[$id_country]['RISK'][] = $item['levelRisk'];

                $arr_disease[$id_disease]['COUNTRIES'][] = $id_country;

            }

            if ($arr_disease) {
                foreach ($arr_disease as $key => $item) {
                    $ELEMENT_ID = $key;  // код элемента
                    $IBLOCK_ID = "11";
                    $PROPERTY_CODE = "COUNTRY";  // код свойства
                    $PROPERTY_VALUE = $item['COUNTRIES'];// значение свойства
                    CIBlockElement::SetPropertyValues($ELEMENT_ID, $IBLOCK_ID, $PROPERTY_VALUE, $PROPERTY_CODE);

                    $PROPERTY_CODE = "COUNTRY_TRANSIT";  // код свойства
                    CIBlockElement::SetPropertyValues($ELEMENT_ID, $IBLOCK_ID, $PROPERTY_VALUE, $PROPERTY_CODE);
                    $mess["disease_country"][] = "Successfully added countries properties for the disease with id {$key}";
                }
            } else {
                $mess["disease_country"][] = "Array is empty or contains errors";
            }

            if ($arr_countries) {
                foreach ($arr_countries as $key => $item) {
                    $ELEMENT_ID = $key;  // код элемента
                    $IBLOCK_ID = "16";
                    $PROPERTY_CODE = "DISEASE";  // код свойства
                    $PROPERTY_VALUE = $item['DISEASE'];// значение свойства
                    CIBlockElement::SetPropertyValues($ELEMENT_ID, $IBLOCK_ID, $PROPERTY_VALUE, $PROPERTY_CODE);
                    $min = min($item['RISK']);
                    if ($min > 0) {
                        if ($min == 1) {
                            $PROPERTY_VALUE = "13";// значение свойства
                            CIBlockElement::SetPropertyValues($ELEMENT_ID, $IBLOCK_ID, $PROPERTY_VALUE, $PROPERTY_CODE);
                        } elseif ($min == 2) {
                            $PROPERTY_VALUE = "14";// значение свойства
                            CIBlockElement::SetPropertyValues($ELEMENT_ID, $IBLOCK_ID, $PROPERTY_VALUE, $PROPERTY_CODE);
                        } else {
                            $mess["risk_country"][] = "Risk values greater than 2";
                        }
                        $PROPERTY_CODE = "RISK";  // код свойства
                    } else {
                        $mess["risk_country"][] = "Risk value the risk of missing";
                    }
                    $mess["risk_country"][] = "Successfully added disease properties for the country with id {$key}. Risk: {$min}.";

                    //Риск в расписание
                    if ($key) {
                        $arSelect = Array("ID");
                        $arFilter = Array("IBLOCK_ID" => 5, "ACTIVE" => "Y", "PROPERTY_DEPARTURE_COUNTRY" => $key);
                        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                        while ($ob = $res->GetNextElement()) {
                            $arFields = $ob->GetFields();
                            $id_country_r[] = $arFields["ID"];
                        }
                    }
                    foreach ($id_country_r as $item){
                        $ELEMENT_ID = $item;  // код элемента
                        $IBLOCK_ID = "5";
                        $PROPERTY_CODE = "RISK";  // код свойства
                        if ($min > 0) {
                            if ($min == 1) {
                                $PROPERTY_VALUE = 9;
                                CIBlockElement::SetPropertyValues($ELEMENT_ID, $IBLOCK_ID, $PROPERTY_VALUE, $PROPERTY_CODE);
                                $mess["risk_timetable"][] = "Risk: {$min}. Element id: {$ELEMENT_ID}";
                            } elseif ($min == 2) {
                                $PROPERTY_VALUE = 10;
                                CIBlockElement::SetPropertyValues($ELEMENT_ID, $IBLOCK_ID, $PROPERTY_VALUE, $PROPERTY_CODE);
                                $mess["risk_timetable"][] = "Risk: {$min}. Element id: {$ELEMENT_ID}";
                            } else {
                                $mess["risk_timetable"][] = "Risk values greater than 2({$min}). Element id: {$ELEMENT_ID}";
                            }
                        } else {
                            $mess["risk_timetable"][] = "The risk value is less than 0({$min}). Element id: {$ELEMENT_ID}";
                        }
                    }
                    $min = 0;
                    unset($id_country_r);
                }
            } else {
                $mess["risk_country"][] = "Array is empty or contains errors";
            }
        }
        //логи ошибок и успха
        if($mess){
            $date = date("m-d-y-H-i-s");
            $path = $_SERVER['DOCUMENT_ROOT']."/logfile/logset/log-set-pcd-".$date.".txt";
            $fp = fopen($path, "w");
            foreach ($mess as $k1 => $m1){
                foreach ($m1 as $m2) {
                    fwrite($fp, "{$k1} - {$m2} \r");
                }
            }
            fclose($fp);
        }
    }


    /**
     * Объединяющая функция для агента
     */
    static function SetCountriesDisease(){
        self::ClearDiseaseWithCountries();
        self::ClearCountriesWithDisease();
        self::ClearTimetableWithRisk();
        self::SetPropertyCountriesDisease();
        return "ClassSetCountriesDisease::SetCountriesDisease();";
    }
}
?>