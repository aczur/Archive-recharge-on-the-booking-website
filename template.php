<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true){die();}
	//вызов класса и обработка в /personal/stats_balance_inc.php
	
	$count_notice=5;//количество в одном выводе.
 
	// создаем объект пагинации 
	$nav = new \Bitrix\Main\UI\PageNavigation("nav-more-notice");
	$nav->allowAllRecords(true)
   ->setPageSize($count_notice)
   ->initFromUri();

//только для групп с административными правами   
$groupID1 = 1;$groupID13 = 13; 
if (in_array($groupID1,$USER->GetUserGroupArray()) or in_array($groupID13,$USER->GetUserGroupArray()))
{ 
	$access_page = 1;
	if(isset($_POST['uid'])){
		$user_id = $_POST['uid'];
	}
	
} else { 
	$access_page = 0;$user_id = $USER->GetID();
}
?>
<div class="balance_content">

<div class="balance_title"><?=GetMessage('STATS_BALANCE_ARCHIVE')?></div>
<? 	
CModule::IncludeModule('highloadblock');
	$entity_data_class = GetEntityDataClass(MY_HL_BLOCK_ID);
	$rsData = $entity_data_class::getList(array(
		'select' => array('*'),
		'order' => array('UF_DATE' => 'ASC'),
		'count_total' => true,
		'offset' => $nav->getOffset(),
		'limit' => $nav->getLimit(),
		'filter' => array('UF_USER_ID' => $user_id)
	));
	$nav->setRecordCount($rsData->getCount());
	while($ell = $rsData->fetch()){
?>
	<div class="balance_box">
	<?if($access_page == 1){?>
	<?=$ell['ID'];?>
		<div class="balance_item"><?=GetMessage('STATS_BALANCE_ID')?><span class="balance_info"><?=$ell['UF_USER_ID'];?></span></div>
	<?}?>
		<div class="balance_item"><?=GetMessage('STATS_BALANCE_LOGIN')?><span class="balance_info"><?=$ell['UF_USER_LOGIN'];?></span></div>
		<div class="balance_item"><?=GetMessage('STATS_BALANCE_BALANCE')?><span class="balance_info">
		<?=$ell['UF_BALANCE'];?><?=GetMessage('STATS_BALANCE_RUB')?></span></div>
		<div class="balance_item"><?=GetMessage('STATS_BALANCE_DATE')?><span class="balance_info"><?=$ell['UF_DATE'];?></span></div>
	</div>
<?
	}
?>
</div>

<?$APPLICATION->IncludeComponent("bitrix:main.pagenavigation", "modern", Array(
	"NAV_OBJECT" => $nav,
		"SEF_MODE" => "N"
	),
	false
);?>
